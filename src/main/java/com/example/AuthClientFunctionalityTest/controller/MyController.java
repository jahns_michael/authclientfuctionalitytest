package com.example.AuthClientFunctionalityTest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;



@Controller
public class MyController {

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String loginPage() {
        return "/index";
    }
    
    @RequestMapping(value="/oauth2/redirect", method=RequestMethod.GET)
    public String loginPage(@RequestParam(value = "token") String accessToken, Model model) {
        model.addAttribute("accessToken", accessToken);
        return "/token";
    }

}