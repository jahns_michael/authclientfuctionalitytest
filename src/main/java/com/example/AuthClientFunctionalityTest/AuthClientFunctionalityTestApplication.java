package com.example.AuthClientFunctionalityTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthClientFunctionalityTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AuthClientFunctionalityTestApplication.class, args);
	}

}
